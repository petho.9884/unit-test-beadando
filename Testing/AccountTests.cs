﻿using Homework;
using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Testing
{
    [TestFixture]
    public class AccountTests
    {
        private Account account;
        private int accId;
        private AccountActivityService service;

        private Mock<IAccountRepository> mockAccRep;
        private Mock<IAction> mockAction;

        [SetUp]
        public void SetUp()
        {
            accId = 1;
            mockAccRep = new Mock<IAccountRepository>();
            mockAction = new Mock<IAction>();
            mockAction.Setup(p => p.Execute()).Returns(true);

            account = new Account(accId);
            service = new AccountActivityService(mockAccRep.Object);
        }

        [Test]
        [Category("Trivial")]
        public void AccountIdTest()
        {
            Assert.That(this.accId, Is.EqualTo(account.Id));
        }

        [Test]
        [Category("Trivial")]
        public void AccountRegistryCorrectTest()
        {
            account.Register();
            Assert.That(account.IsRegistered, Is.True);
        }

        [Test]
        [Category("Trivial")]
        public void AccountActivateTest()
        {
            account.Activate();
            Assert.That(account.IsConfirmed, Is.True);
        }

        [Test]
        [Category("Account")]
        public void AccountIsInactiveTest()
        {
            Account newAcc = new Account(this.accId + 1);
            Assert.That(() => newAcc.TakeAction(mockAction.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        [Category("Account")]
        [TestCase(true)]
        [TestCase(false)]
        public void AccountActionBehaviourTest(bool outcomeExpected)
        {
            account.Register();
            account.Activate();
            mockAction.Setup(p => p.Execute()).Returns(outcomeExpected);

            if (outcomeExpected)
            {
                Assert.That(() => account.TakeAction(mockAction.Object), Is.True);
            }
            else
            {
                Assert.That(() => account.TakeAction(mockAction.Object), Is.False);
            }
            
        }

        [Test]
        [Category("Account")]
        [TestCase(true)]
        [TestCase(false)]
        public void AccountActionTest(bool isAccInactive)
        {
            if (isAccInactive)
            {
                Assert.That(() => account.TakeAction(mockAction.Object), Throws.TypeOf<InactiveUserException>());
            }
            else
            {
                account.Register();
                account.Activate();
                Assert.That(account.TakeAction(mockAction.Object), Is.True);
            }
        }

        [Test]
        [Category("Service")]
        public void ServiceAccountNotExistsTest()
        {
            Assert.That(() => service.GetActivity(69), Throws.TypeOf<AccountNotExistsException>());
        }

        [Test]
        [Category("Service")]
        [TestCase(0,0)]
        [TestCase(11, 1)]
        [TestCase(22, 2)]
        [TestCase(44, 3)]
        public void ServiceActivityTest(int actionNum, int actionExpected)
        {
            account.Activate();
            account.Register();
            mockAccRep.Setup(p => p.Get(It.IsAny<int>())).Returns(account);

            for (int i = 0; i < actionNum; i++)
            {
                account.TakeAction(mockAction.Object);
            }

            var activity = service.GetActivity(accId);

            Assert.That(activity, Is.EqualTo((ActivityLevel)actionExpected));

            mockAccRep.Verify(p => p.Get(It.IsAny<int>()), Times.Once);
        }

        [Test]
        [Category("Service")]
        public void ServiceActionOnNonexistentAccountTest()
        {
            Assert.That(() => service.GetActivity(69), Throws.TypeOf<AccountNotExistsException>());
        }
    }
}
